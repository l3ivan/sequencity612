##Sequencity612 dataset

For our experiments, we propose a dataset called Sequencity612. It is extracted from the online comic book library [Sequencity](https://www.sequencity.com).  This library is composed of 140000 pages of comics from europe/america and mangas from Japan. We randomly selected 1000 comics pages and removed pages containing covers, preface, and pages without comic characters. There were 612 pages left where at least one character appears. We made ground truth bounding boxes for all characters, small or big, speaking or not and in the background. Because the Sequencity comics are private, we can not provide the images, but we provide the ground truth and corresponding album names and page numbers for anyone who want to access the images. The three image lists for the training set, validating set and the testing set are publicly available. To our knowledge, this is the largest dataset for comic characters.
In order to create the ground-truth for Sequencity612, we have identified five different types of characters that we classified into: human-like (classid = 0), near human-like (classid = 1), far human-like (classid = 2), object-or-animal-like (classid = 3) and manga characters (classid = 4) (characters from japanese mangas). We also identified very small characters in the background and named it "figurant" with classid=5 the ground-truth for panels, with classid = 6 and for faces with classid = 7. In the MANUPU paper, we have experimented with 5 character classes (classid from 0 to 4)

The label text file has each line for one object, each object is described by a bounding box, with the format:

```
classid xmin ymin xmax ymax
```

Follow the book id and the image names in the file "Sequencity612.txt" you can download the dataset (images) if you have access to [Sequencity](https://www.sequencity.com)
